unit UnOptionFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, UnConfiguration;

type
  TFmOptions = class(TForm)
    btnSave: TBitBtn;
    btnCancel: TBitBtn;
    grpGeneral: TGroupBox;
    chkAutoStart: TCheckBox;
    chkAlwaysOnTop: TCheckBox;
    chkWindowToForeground: TCheckBox;
    chkPlaySoundResize: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fConfiguration: TConfiguration;
  public
    constructor Create(aOwner: TComponent; aConfiguration: TConfiguration); reintroduce;
  end;

implementation

{$R *.dfm}

{ TFmOptions }

procedure TFmOptions.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFmOptions.btnSaveClick(Sender: TObject);
begin
  fConfiguration.AutoStart := chkAutoStart.Checked;
  fConfiguration.WindowAlwaysTop := chkAlwaysOnTop.Checked;
  fConfiguration.PlaySoundOnAction := chkPlaySoundResize.Checked;
  ModalResult := mrOk;
end;

constructor TFmOptions.Create(aOwner: TComponent; aConfiguration: TConfiguration);
begin
  inherited Create(aOwner);
  if Assigned(aConfiguration) then
    fConfiguration := aConfiguration
  else
    raise EArgumentNilException.Create('TConfiguration is nil');
end;

procedure TFmOptions.FormActivate(Sender: TObject);
begin
  chkAutoStart.Checked := fConfiguration.AutoStart;
  chkAlwaysOnTop.Checked := fConfiguration.WindowAlwaysTop;
  chkPlaySoundResize.Checked := fConfiguration.PlaySoundOnAction;
end;

procedure TFmOptions.FormShow(Sender: TObject);
begin
  if Assigned(PopupParent) then
  begin
    Left := PopupParent.Left + (PopupParent.Width div 2) - (Width div 2);
    Top  := PopupParent.Top + (PopupParent.Height div 2) - (Height div 2);
  end;
end;

end.
