unit UnTargetOption;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask,
  Vcl.ExtCtrls, Vcl.Buttons, UnTargetOptionData;

type
  TFmTargetOption = class(TForm)
    rbStay: TRadioButton;
    rbPosition: TRadioButton;
    rbCenter: TRadioButton;
    cbbAction: TComboBox;
    chkSizeChange: TCheckBox;
    lblAction: TLabel;
    lblSizeMask: TLabel;
    edSizeX: TEdit;
    edSizeY: TEdit;
    edPositionX: TEdit;
    edPositionY: TEdit;
    lblPositonMask: TLabel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    edWindowTitle: TLabeledEdit;
    edClassName: TLabeledEdit;
    grpSearchOptions: TGroupBox;
    rbClassCompare: TRadioButton;
    rbTitleCompare: TRadioButton;
    chkAlwaysTop: TCheckBox;
    edExecutable: TLabeledEdit;
    rbExeCompare: TRadioButton;
    lblAdminWarning: TLabel;
    procedure cbbActionChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    fTargetOptions: TTargetOptionData;
  public
    property TargetOptions: TTargetOptionData read fTargetOptions;
    constructor Create(aOwner: TComponent; aTargetOptions: TTargetOptionData); reintroduce;
  end;

implementation

{$R *.dfm}

{TFmTargetOption}

procedure TFmTargetOption.btnOKClick(Sender: TObject);
begin
  fTargetOptions.Title := edWindowTitle.Text;
  fTargetOptions.WindowClass := edClassName.Text;
  fTargetOptions.ExecutableName := edExecutable.Text;

  case cbbAction.ItemIndex of
    0: fTargetOptions.Action := TWindowAction.PositionResize;
    1: fTargetOptions.Action := TWindowAction.Maximize;
    2: fTargetOptions.Action := TWindowAction.Minimize;
  end;

  if rbTitleCompare.Checked then
    fTargetOptions.CompareMethod := TCompareMethod.Title
  else if rbClassCompare.Checked then
    fTargetOptions.CompareMethod := TCompareMethod.ClassName
  else
    fTargetOptions.CompareMethod := TCompareMethod.Executable;

  fTargetOptions.SetSize := chkSizeChange.Checked;
  fTargetOptions.SizeWidth := StrToInt(edSizeX.Text);
  fTargetOptions.SizeHeight := StrToInt(edSizeY.Text);
  fTargetOptions.AlwaysOnTop := chkAlwaysTop.Checked;

  if rbPosition.Checked then
  begin
    fTargetOptions.PositionOptions.ChangePosition := True;
    fTargetOptions.PositionOptions.PositionPt := TPoint.Create(StrToInt(edPositionX.Text),StrToInt(edPositionY.Text));
  end
  else if rbCenter.Checked then
    fTargetOptions.PositionOptions.Center := True
  else
  begin
    fTargetOptions.PositionOptions.ChangePosition := False;
    fTargetOptions.PositionOptions.Center := False;
  end;
end;

procedure TFmTargetOption.cbbActionChange(Sender: TObject);
var
  lShowPosOption: Boolean;
begin
  lShowPosOption := cbbAction.ItemIndex = 0;
  rbStay.Visible := lShowPosOption;
  rbPosition.Visible := lShowPosOption;
  rbCenter.Visible := lShowPosOption;
  rbPosition.Checked := lShowPosOption;
  edPositionX.Visible := lShowPosOption;
  edPositionY.Visible := lShowPosOption;
  lblPositonMask.Visible := lShowPosOption;
  rbCenter.Checked := False;
  rbPosition.Checked := False;
  rbStay.Checked := True;
end;

constructor TFmTargetOption.Create(aOwner: TComponent; aTargetOptions: TTargetOptionData);
begin
  inherited Create(aOwner);
  if Assigned(aTargetOptions) then
    fTargetOptions := aTargetOptions
  else
    raise EArgumentNilException.Create('TTargetOptionData is nil');
end;

procedure TFmTargetOption.FormActivate(Sender: TObject);
begin
  lblAdminWarning.Visible := False;
  case fTargetOptions.Action of
    PositionResize:
    begin
      cbbAction.ItemIndex := 0;
      edPositionX.Text := IntToStr(fTargetOptions.PositionOptions.PositionPt.X);
      edPositionY.Text := IntToStr(fTargetOptions.PositionOptions.PositionPt.Y);
      if fTargetOptions.PositionOptions.ChangePosition then
        rbPosition.Checked := True
      else if fTargetOptions.PositionOptions.Center then
        rbCenter.Checked := True
      else
        rbStay.Checked := True;
    end;
    Maximize:
      cbbAction.ItemIndex := 1;
    Minimize:
      cbbAction.ItemIndex := 2;
  end;
  edPositionX.Visible := fTargetOptions.Action = TWindowAction.PositionResize;
  edPositionY.Visible := fTargetOptions.Action = TWindowAction.PositionResize;
  lblPositonMask.Visible := fTargetOptions.Action = TWindowAction.PositionResize;
  edSizeX.Text := IntToStr(fTargetOptions.SizeWidth);
  edSizeY.Text := IntToStr(fTargetOptions.SizeHeight);
  chkSizeChange.Checked := fTargetOptions.SetSize;
  edWindowTitle.Text := fTargetOptions.Title;
  edClassName.Text := fTargetOptions.WindowClass;
  edExecutable.Text := fTargetOptions.ExecutableName;
  chkAlwaysTop.Checked :=  fTargetOptions.AlwaysOnTop;
  if fTargetOptions.CompareMethod = TCompareMethod.Title then
    rbTitleCompare.Checked := True
  else if fTargetOptions.CompareMethod = TCompareMethod.ClassName then
    rbClassCompare.Checked := True
  else
    rbExeCompare.Checked := True;

  if fTargetOptions.ExecutableName = 'Access Denied' then
  begin
    edExecutable.Enabled := False;
    rbExeCompare.Enabled := False;
    lblAdminWarning.Visible := True;
  end;

end;

procedure TFmTargetOption.FormShow(Sender: TObject);
begin
  if Assigned(PopupParent) then
  begin
    Left := PopupParent.Left + (PopupParent.Width div 2) - (Width div 2);
    Top  := PopupParent.Top + (PopupParent.Height div 2) - (Height div 2);
  end;
end;

end.
