unit UnMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ImgList, System.ImageList, Generics.Collections, UnWindowData, Vcl.ExtCtrls,
  Vcl.Menus, UnHelperFunctions, UnTargetOption, UnTargetOptionData, UnOptionFrame, UnConfiguration;

type
  TFmMain = class(TForm)
    grpOpenWindows: TGroupBox;
    grpSelectedWindows: TGroupBox;
    lvOpenWindows: TListView;
    lvSelectedWindows: TListView;
    btnSelectWindow: TBitBtn;
    btnEdit: TBitBtn;
    btnRemove: TBitBtn;
    ilIcons: TImageList;
    trycnMain: TTrayIcon;
    pmTrayMenu: TPopupMenu;
    meEnabled: TMenuItem;
    meExit: TMenuItem;
    meOpen: TMenuItem;
    N1: TMenuItem;
    meAutoSizeNow: TMenuItem;
    N2: TMenuItem;
    btnReload: TBitBtn;
    btnOptions: TBitBtn;
    btnHide: TBitBtn;
    btnExit: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnReloadClick(Sender: TObject);
    procedure btnSelectWindowClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure meAutoSizeNowClick(Sender: TObject);
    procedure trycnMainDblClick(Sender: TObject);
    procedure meOpenClick(Sender: TObject);
    procedure meExitClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure meEnabledClick(Sender: TObject);
    procedure lvOpenWindowsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure lvSelectedWindowsChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure btnHideClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOptionsClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  strict private
    fWindows: TObjectList<TWindowData>;
    fTargets: TObjectList<TTargetOptionData>;
    procedure InitHook();
    procedure RemoveHook();
    procedure OnMinimize(Sender: TObject);
    procedure ReloadTargetView();
  public
    property Windows: TObjectList<TWindowData> read fWindows;
    property Targets: TObjectList<TTargetOptionData> read fTargets;
    procedure UpdateWindowList(aReloadAll: Boolean = True);
    procedure ProcessConfiguration();
  end;

const
  OptionSaveFile: string = '\AutoSizeIt\config.xml';
  TargetSaveFile: string = '\AutoSizeIt\targets.xml';

var
  FmMain: TFmMain;
  fHook: HHOOK;
  gConfiguration: TConfiguration;

procedure WindowEventHookProc(hWinEventHook: HHOOK; event: DWORD; hwnd: HWND; idObject: LONG; idChild: LONG; idEventThread: DWORD; dwmsEventTime: DWORD);

implementation

procedure WindowEventHookProc(hWinEventHook: HHOOK; event: DWORD; hwnd: HWND; idObject: LONG; idChild: LONG; idEventThread: DWORD; dwmsEventTime: DWORD);
var
  lWindow: TWindowData;
  lTarget: TTargetOptionData;
begin
  if (event = EVENT_SYSTEM_FOREGROUND) then
  begin
    if not IsWindow(hwnd) then
      exit;
    lWindow := TWindowData.Create(hwnd);
    for lTarget in FmMain.Targets do
    begin
      if lWindow.IsTarget(lTarget) then
      begin
        lWindow.MatchTargetValues(lTarget, gConfiguration.PlaySoundOnAction);
        break;
      end;
    end;
  end;
end;

{$R *.dfm}

{ TFmMain }

procedure TFmMain.btnEditClick(Sender: TObject);
var
  i: Integer;
  lTargetOptions: TTargetOptionData;
  lTargetOptionForm: TFmTargetOption;
  lItem: TListItem;
  lWindow: TWindowData;
begin
  for i := lvSelectedWindows.Items.Count - 1 downto 0 do
    if lvSelectedWindows.Items[i].Selected then
      for lTargetOptions in fTargets do
        if lTargetOptions.Title = lvSelectedWindows.Items[i].Caption then
        begin
          lTargetOptionForm := TFmTargetOption.Create(Self, lTargetOptions);
          lTargetOptionForm.PopupParent := Self;
          if lTargetOptionForm.ShowModal = mrOk then
          begin
            lItem := lvSelectedWindows.Items[i];
            lItem.Caption := lTargetOptions.Title;
            lItem.SubItems.Clear;
            lItem.SubItems.Add(lTargetOptions.WindowClass);
            lItem.SubItems.Add(lTargetOptions.ActionString);
            lItem.SubItems.Add(lTargetOptions.MethodString);
            lWindow := FindWindowData(fWindows, lTargetOptions);
            if Assigned(lWindow) and lWindow.Valid then
              lWindow.MatchTargetValues(lTargetOptions);
          end;
          lTargetOptionForm.Free;
          Break;
        end;
end;

procedure TFmMain.btnHideClick(Sender: TObject);
begin
  Application.Minimize;
  Self.WindowState := wsMinimized;
end;

procedure TFmMain.btnOptionsClick(Sender: TObject);
var
  lOptionFrame: TFmOptions;
begin
  gConfiguration.Load(GetMyDocuments + OptionSaveFile);
  lOptionFrame := TFmOptions.Create(Self, gConfiguration);
  lOptionFrame.PopupParent := Self;
  if lOptionFrame.ShowModal = mrOk then
  begin
    gConfiguration.Save(GetMyDocuments + OptionSaveFile);
    ProcessConfiguration();
  end;
  lOptionFrame.Free;
end;

procedure TFmMain.btnReloadClick(Sender: TObject);
begin
  UpdateWindowList();
end;

procedure TFmMain.btnRemoveClick(Sender: TObject);
var
  i: Integer;
  lTarget: TTargetOptionData;
  lWindow: TWindowData;
begin
  lvSelectedWindows.Items.BeginUpdate;
  try
    for i := lvSelectedWindows.Items.Count - 1 downto 0 do
      if lvSelectedWindows.Items[i].Selected then
      begin
        for lTarget in fTargets do
        begin
          if lTarget.Title.Equals(lvSelectedWindows.Items[i].Caption) then
          begin
            if fTargets.Items[i].AlwaysOnTop then
            begin
              lWindow := FindWindowData(fWindows, fTargets.Items[i].Title, fTargets.Items[i].WindowClass);
              if lWindow.Valid then
                RemoveAlwaysOnTop(lWindow.WndHnd);
            end;
            break;
          end;
        end;
        lvSelectedWindows.Items[i].Delete;
      end;
    if lvSelectedWindows.Items.Count = 0 then
      fTargets.Clear;
  finally
    lvSelectedWindows.Items.EndUpdate;
  end;
end;

procedure TFmMain.btnSelectWindowClick(Sender: TObject);
var
  i: Integer;
  lTargetOptions: TTargetOptionData;
  lTargetOptionForm: TFmTargetOption;
  lWindowData: TWindowData;
  lItem: TListItem;
begin
  lvSelectedWindows.Items.BeginUpdate;
  try
    for i := lvOpenWindows.Items.Count - 1 downto 0 do
      if lvOpenWindows.Items[i].Selected then
      begin
        lvOpenWindows.Items[i].Selected := False;
        lTargetOptions := TTargetOptionData.Create;
        lWindowData := FindWindowData(fWindows, lvOpenWindows.Items[i].Caption, lvOpenWindows.Items[i].SubItems[0]);
        lTargetOptions.Title := lWindowData.Title;
        lTargetOptions.WindowClass := lWindowData.WindowClass;
        lTargetOptions.ExecutableName := lWindowData.ExecutableName;
        lTargetOptions.PositionOptions.PositionPt := lWindowData.Position;
        lTargetOptions.SizeWidth := lWindowData.SizeWidth;
        lTargetOptions.SizeHeight := lWindowData.SizeHeight;
        lTargetOptionForm := TFmTargetOption.Create(FmMain, lTargetOptions);
        lTargetOptionForm.PopupParent := Self;
        if lTargetOptionForm.ShowModal = mrOk then
        begin
          lItem := lvSelectedWindows.Items.Add;
          lItem.ImageIndex := -1;
          lItem.Caption := lTargetOptions.Title;
          lItem.SubItems.Add(lTargetOptions.WindowClass);
          lItem.SubItems.Add(lTargetOptions.ActionString);
          lItem.SubItems.Add(lTargetOptions.MethodString);
          lWindowData.MatchTargetValues(lTargetOptions, gConfiguration.PlaySoundOnAction);
          fTargets.Add(lTargetOptions);
        end
        else
          lTargetOptions.Free;
        lTargetOptionForm.Free;
      end;
  finally
    lvSelectedWindows.Items.EndUpdate;
  end;
end;

procedure TFmMain.FormActivate(Sender: TObject);
begin
  Constraints.MaxHeight := Height;
  Constraints.MinHeight := Height;
  Constraints.MaxWidth := Width;
  Constraints.MinWidth := Width;
  ProcessConfiguration();
  UpdateWindowList();
  InitHook();
end;

procedure TFmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
{$IFNDEF DEBUG}
  Application.Minimize;
  CanClose := False;
  Self.WindowState := wsMinimized;
{$ENDIF}
end;

procedure TFmMain.FormCreate(Sender: TObject);
begin
  fWindows := TObjectList<TWindowData>.Create();
  fTargets := TObjectList<TTargetOptionData>.Create();
  gConfiguration := TConfiguration.Create;
  Application.OnMinimize := OnMinimize;

  gConfiguration.Load(GetMyDocuments + OptionSaveFile);
  LoadTargetSet(GetMyDocuments + TargetSaveFile, fTargets);
  ReloadTargetView();
end;

procedure TFmMain.FormDestroy(Sender: TObject);
begin
  SaveTargetSet(GetMyDocuments + TargetSaveFile, fTargets);
  fWindows.Free;
  fTargets.Free;
  gConfiguration.Free;
  RemoveHook();
end;

procedure TFmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Shift = [ssCtrl] then
  begin
    if Key = Ord('Q') then
      Application.Terminate()
    else if Key = Ord('R') then
      btnReload.Click;
  end
  else if Shift = [ssAlt] then
    if Key = VK_F4 then
      Application.Terminate;
  if Key = VK_F5 then
    btnReload.Click;
end;

procedure TFmMain.FormShow(Sender: TObject);
begin
  Left := (Screen.Width - Width)  div 2;
  Top := (Screen.Height - Height) div 2;
end;

procedure TFmMain.InitHook;
begin
  if fHook <> 0 then
    RemoveHook;
  fHook := SetWinEventHook(EVENT_SYSTEM_FOREGROUND , EVENT_SYSTEM_FOREGROUND, 0, @WindowEventHookProc, 0, 0, WINEVENT_OUTOFCONTEXT);
  if fHook = 0 then
    RaiseLastOSError;
end;

procedure TFmMain.ReloadTargetView;
var
  lTarget: TTargetOptionData;
  lItem: TListItem;
begin
  lvSelectedWindows.Clear;
  for lTarget in fTargets do
  begin
    lItem := lvSelectedWindows.Items.Add;
    lItem.Caption := lTarget.Title;
    lItem.SubItems.Clear;
    lItem.SubItems.Add(lTarget.WindowClass);
    lItem.SubItems.Add(lTarget.ActionString);
    lItem.SubItems.Add(lTarget.MethodString);
    lItem.ImageIndex := -1;
  end;
end;

procedure TFmMain.lvOpenWindowsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if Change = TItemChange.ctState then
  begin
    btnSelectWindow.Enabled := Item.Selected;
  end;
end;

procedure TFmMain.lvSelectedWindowsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if Change = TItemChange.ctState then
  begin
    btnEdit.Enabled := Item.Selected;
    btnRemove.Enabled := Item.Selected;
  end;
end;

procedure TFmMain.meAutoSizeNowClick(Sender: TObject);
begin
  UpdateWindowList();
end;

procedure TFmMain.meEnabledClick(Sender: TObject);
var
  lMenuItem: TMenuItem;
begin
  lMenuItem := Sender as TMenuItem;
  lMenuItem.Checked := not lMenuItem.Checked;
  if lMenuItem.Checked then
    InitHook()
  else
    RemoveHook();
end;

procedure TFmMain.meExitClick(Sender: TObject);
begin
  Application.Terminate();
end;

procedure TFmMain.meOpenClick(Sender: TObject);
begin
  if Not Visible then
  begin
    Show();
    WindowState := wsNormal;
    Application.BringToFront();
    FormActivate(Sender);
  end
  else
    Application.BringToFront();
end;

procedure TFmMain.OnMinimize(Sender: TObject);
begin
  Hide();
  WindowState := wsMinimized;
end;

procedure TFmMain.ProcessConfiguration();
begin
  if gConfiguration.WindowAlwaysTop then
    SetAlwaysOnTop(Handle)
  else
    RemoveAlwaysOnTop(Handle);
  gConfiguration.SetAutoStart(ParamStr(0), 'AutoSizeIt!', gConfiguration.AutoStart);
end;

procedure TFmMain.RemoveHook;
begin
  if fHook <> 0 then
    UnhookWindowsHookEx(fHook);
end;

procedure TFmMain.trycnMainDblClick(Sender: TObject);
begin
  if Not Visible then
  begin
    Show();
    WindowState := wsNormal;
    Application.BringToFront();
    FormActivate(Sender);
  end
  else
    Application.BringToFront();
end;

procedure TFmMain.UpdateWindowList(aReloadAll: Boolean = True);

  function IsAlreadySelected(lWindow: TWindowData): Boolean;
  var
    i: Integer;
    lItem: TListItem;
  begin
    Result := False;
    for i := lvSelectedWindows.Items.Count - 1 downto 0 do
    begin
      lItem := lvSelectedWindows.Items[i];
      if (lItem.Caption = lWindow.Title) and (lItem.SubItems[0] = lWindow.WindowClass) then
      begin
        Exit(True);
      end;
    end;
  end;

var
  i: Integer;
  lItem: TListItem;
begin
  if aReloadAll then
    fWindows.Clear;
  ilIcons.Clear;
  EnumWindows(@EnumWindowsProc, LongInt(fWindows));

  lvOpenWindows.Items.BeginUpdate;
  lvOpenWindows.Clear;
  for i := 0 to fWindows.Count-1 do
  begin
    ilIcons.AddIcon(fWindows[i].Icon);
    lItem := lvOpenWindows.Items.Add;
    lItem.Caption := fWindows[i].Title;
    lItem.SubItems.Add(fWindows[i].WindowClass);
    lItem.ImageIndex := ilIcons.Count-1;
  end;
  lvOpenWindows.Items.EndUpdate;
end;

end.
