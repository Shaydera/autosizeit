object FmOptions: TFmOptions
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Options'
  ClientHeight = 242
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnSave: TBitBtn
    Left = 8
    Top = 206
    Width = 81
    Height = 28
    Caption = '&Save'
    TabOrder = 0
    OnClick = btnSaveClick
  end
  object btnCancel: TBitBtn
    Left = 259
    Top = 206
    Width = 81
    Height = 28
    Caption = 'C&ancel'
    TabOrder = 1
    OnClick = btnCancelClick
  end
  object grpGeneral: TGroupBox
    Left = 8
    Top = 8
    Width = 355
    Height = 185
    Caption = 'General'
    TabOrder = 2
    object chkAutoStart: TCheckBox
      Left = 8
      Top = 32
      Width = 249
      Height = 17
      Caption = 'Load AutoSizeIt! automatically with Windows'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chkAlwaysOnTop: TCheckBox
      Left = 8
      Top = 72
      Width = 249
      Height = 17
      Caption = 'Keep AutoSizeIt! main window on top of others'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkWindowToForeground: TCheckBox
      Left = 8
      Top = 152
      Width = 313
      Height = 17
      Caption = 'Automatically bring AutoSized windows to the foreground'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 3
    end
    object chkPlaySoundResize: TCheckBox
      Left = 8
      Top = 112
      Width = 313
      Height = 17
      Caption = 'Play the default system sound after a window is AutoSized'
      Enabled = False
      TabOrder = 2
    end
  end
end
