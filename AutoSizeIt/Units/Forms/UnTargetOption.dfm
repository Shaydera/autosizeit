object FmTargetOption: TFmTargetOption
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'AutoSize'
  ClientHeight = 351
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblAction: TLabel
    Left = 8
    Top = 20
    Width = 88
    Height = 13
    Caption = 'Action to perform:'
  end
  object lblSizeMask: TLabel
    Left = 140
    Top = 81
    Width = 6
    Height = 13
    Caption = 'X'
  end
  object lblPositonMask: TLabel
    Left = 167
    Top = 189
    Width = 6
    Height = 13
    Caption = 'X'
  end
  object lblAdminWarning: TLabel
    Left = 267
    Top = 292
    Width = 201
    Height = 26
    Caption = 
      'AutoSizeIt! needs Administrator Privileges'#13#10'for comparison by ex' +
      'ecutable.'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  object rbStay: TRadioButton
    Left = 8
    Top = 157
    Width = 113
    Height = 17
    Caption = 'Don'#39't reposition'
    TabOrder = 5
  end
  object rbPosition: TRadioButton
    Left = 8
    Top = 188
    Width = 113
    Height = 17
    Caption = 'Set position'
    Checked = True
    TabOrder = 6
    TabStop = True
  end
  object rbCenter: TRadioButton
    Left = 8
    Top = 219
    Width = 113
    Height = 17
    Caption = 'Center on screen'
    TabOrder = 7
  end
  object cbbAction: TComboBox
    Left = 8
    Top = 36
    Width = 129
    Height = 21
    AutoDropDown = True
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 0
    Text = 'Resize / Position'
    OnChange = cbbActionChange
    Items.Strings = (
      'Resize / Position'
      'Maximize'
      'Minimize')
  end
  object chkSizeChange: TCheckBox
    Left = 8
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Set size'
    TabOrder = 1
  end
  object edSizeX: TEdit
    Left = 84
    Top = 78
    Width = 50
    Height = 21
    MaxLength = 4
    TabOrder = 2
    Text = '0'
  end
  object edSizeY: TEdit
    Left = 151
    Top = 78
    Width = 50
    Height = 21
    MaxLength = 4
    TabOrder = 3
    Text = '0'
  end
  object edPositionX: TEdit
    Left = 108
    Top = 186
    Width = 50
    Height = 21
    MaxLength = 4
    TabOrder = 8
    Text = '0'
  end
  object edPositionY: TEdit
    Left = 180
    Top = 186
    Width = 50
    Height = 21
    MaxLength = 4
    TabOrder = 9
    Text = '0'
  end
  object btnOK: TBitBtn
    Left = 8
    Top = 308
    Width = 75
    Height = 25
    Caption = '&OK'
    ModalResult = 1
    TabOrder = 10
    OnClick = btnOKClick
  end
  object btnCancel: TBitBtn
    Left = 140
    Top = 308
    Width = 75
    Height = 25
    Caption = 'C&ancel'
    ModalResult = 2
    TabOrder = 11
  end
  object edWindowTitle: TLabeledEdit
    Left = 264
    Top = 36
    Width = 249
    Height = 21
    EditLabel.Width = 63
    EditLabel.Height = 13
    EditLabel.Caption = 'Window title:'
    TabOrder = 12
  end
  object edClassName: TLabeledEdit
    Left = 264
    Top = 89
    Width = 249
    Height = 21
    EditLabel.Width = 58
    EditLabel.Height = 13
    EditLabel.Caption = 'Class name:'
    TabOrder = 13
  end
  object grpSearchOptions: TGroupBox
    Left = 264
    Top = 188
    Width = 233
    Height = 98
    Caption = 'Window searching options'
    TabOrder = 14
    object rbClassCompare: TRadioButton
      Left = 3
      Top = 46
      Width = 230
      Height = 17
      Caption = 'Compare using class name'
      TabOrder = 1
    end
    object rbTitleCompare: TRadioButton
      Left = 3
      Top = 23
      Width = 230
      Height = 17
      Caption = 'Compare using window title (Default)'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object rbExeCompare: TRadioButton
      Left = 3
      Top = 69
      Width = 230
      Height = 17
      Caption = 'Compare using executable'
      TabOrder = 2
    end
  end
  object chkAlwaysTop: TCheckBox
    Left = 8
    Top = 113
    Width = 201
    Height = 17
    Caption = 'Keep window always on top of others'
    TabOrder = 4
  end
  object edExecutable: TLabeledEdit
    Left = 265
    Top = 137
    Width = 249
    Height = 21
    EditLabel.Width = 53
    EditLabel.Height = 13
    EditLabel.Caption = 'Executable'
    TabOrder = 15
  end
end
