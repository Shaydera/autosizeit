unit UnWindowData;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  UnTargetOptionData, UnWinApiExtensions;

type
  TWindowData = class
  strict private
    fWndHnd: HWND;
    fTitle,
    fWindowClass,
    fExecutableName: string;
    function GetTitle(): string;
    function GetWindowClass(): string;
    function GetExecutable(): string;
    function GetWindowIcon(): TIcon;
    function GetWindowPosition(): TPoint;
    procedure SetWindowPosition(aValue: TPoint);
    function GetWindowSize(aIndex: Integer): Integer;
    procedure SetWindowSize(aIndex: Integer; aValue: Integer);
    function IsValidWindow(): Boolean;
  public
    property WndHnd: HWND read fWndHnd;
    property Title: string read GetTitle;
    property WindowClass: string read GetWindowClass;
    property ExecutableName: string read GetExecutable;
    property Icon: TIcon read GetWindowIcon;
    property Position: TPoint read GetWindowPosition write SetWindowPosition;
    property SizeWidth: Integer Index 0 read GetWindowSize write SetWindowSize;
    property SizeHeight: Integer Index 1 read GetWindowSize write SetWindowSize;
    property Valid: Boolean read IsValidWindow;
    constructor Create(aWnd :HWND); overload;
    function IsTarget(aTarget: TTargetOptionData): Boolean;
    procedure MatchTargetValues(aTarget: TTargetOptionData; aPlaySound: Boolean = False);
    procedure Reload();
  end;

implementation

{ TWindowData }

constructor TWindowData.Create(aWnd: HWND);
begin
  inherited Create;
  if IsWindow(aWnd) then
    fWndHnd := aWnd
  else
    raise EInvalidOperation.Create('Provided handle is not a valid Window');
end;

function TWindowData.GetExecutable: string;
var
  lBuffer: array[0..MAX_PATH] of Char;
  lModulePath: string;
  lPid: Cardinal;
  lProcess: THandle;
begin
  if fExecutableName = '' then
  begin
    GetWindowThreadProcessId(fWndHnd, lPid); //Retrieve ProcessId of the Window beforehand
    lProcess := OpenProcess(PROCESS_QUERY_INFORMATION, False, lPid);
    if lProcess > 0 then
    begin
      FillChar(lBuffer, sizeOf(lBuffer), 0);
      try
        if not GetModuleFileNameExW(lProcess, 0, lBuffer, MAX_PATH) then
          RaiseLastOSError;
      finally
        CloseHandle(lProcess)
      end;
      lModulePath := ExtractFileName(string(lBuffer));
    end
    else if GetLastError = 5 then
      lModulePath := 'Access Denied';

    fExecutableName := lModulePath
  end;
  Result := fExecutableName;
end;

function TWindowData.GetTitle: string;
var
  lWindowTitle: array[0..127] of Char;
begin
  if fTitle = '' then
  begin
    FillChar(lWindowTitle, sizeOf(lWindowTitle), 0);
    GetWindowText(fWndHnd, lWindowTitle, 128);
    fTitle := lWindowTitle;
  end;
  Result := fTitle;
end;

function TWindowData.GetWindowClass: string;
var
  lWindowClass: array[0..127] of Char;
begin
  if fWindowClass = '' then
  begin
    FillChar(lWindowClass, sizeOf(lWindowClass), 0);
    GetClassName(fWndHnd, lWindowClass, 128);
    fWindowClass := lWindowClass;
  end;
  Result := fWindowClass;
end;

function TWindowData.GetWindowIcon: TIcon;
var
  lIcoHnd: HWND;
begin
  lIcoHnd := SendMessage(fWndHnd,WM_GETICON,ICON_SMALL,0);
  if (lIcoHnd = 0) then
    lIcoHnd := SendMessage(fWndHnd,WM_GETICON,ICON_BIG,0);
  if (lIcoHnd = 0) then
    lIcoHnd := GetClassLongPtr(fWndHnd, GCL_HICONSM);
  if (lIcoHnd = 0) then
    lIcoHnd := GetClassLongPtr(fWndHnd, GCL_HICON);

  if (lIcoHnd = 0) then
    exit(nil);

  Result := TIcon.Create;
  Result.Handle := lIcoHnd;
end;

function TWindowData.GetWindowPosition: TPoint;
var
  lRect: TRect;
begin
  GetWindowRect(fWndHnd,lRect);
  if lRect.Top > -30000 then
    Result := TPoint.Create(lRect.TopLeft)
  else
    Result := TPoint.Create(0,0);
end;

function TWindowData.GetWindowSize(aIndex: Integer): Integer;
var
  lRect: TRect;
begin
  GetWindowRect(fWndHnd,lRect);
  if aIndex = 0 then
    Result := lRect.Right - lRect.Left
  else
    Result := lRect.Bottom - lRect.Top;
end;

function TWindowData.IsTarget(aTarget: TTargetOptionData): Boolean;
begin
  Result := False;
  case aTarget.CompareMethod of
    TCompareMethod.Title: Result := GetTitle.Contains(aTarget.Title);
    TCompareMethod.ClassName: Result := GetWindowClass.Equals(aTarget.WindowClass);
    TCompareMethod.Executable: Result := GetExecutable.Equals(aTarget.ExecutableName);
  end;
end;

function TWindowData.IsValidWindow: Boolean;
begin
  Result := (fWndHnd > 0) and IsWindow(fWndHnd);
end;

procedure TWindowData.MatchTargetValues(aTarget: TTargetOptionData; aPlaySound: Boolean = False);
var
  lScreenWidth, lScreenHeight: Integer;
begin
  case aTarget.Action of
    TWindowAction.PositionResize:
      begin
        if aTarget.PositionOptions.ChangePosition then
        begin
          Self.Position := aTarget.PositionOptions.PositionPt;
        end
        else if aTarget.PositionOptions.Center then
        begin
          lScreenWidth := GetSystemMetrics(SM_CXSCREEN);
          lScreenHeight := GetSystemMetrics(SM_CYSCREEN);
          Self.Position := TPoint.Create((lScreenWidth - Self.SizeWidth) div 2, (lScreenHeight - Self.SizeHeight) div 2);
        end;
      end;
    TWindowAction.Maximize:
        ShowWindow(fWndHnd, SW_MAXIMIZE);
    TWindowAction.Minimize:
        ShowWindow(fWndHnd, SW_MINIMIZE);
  end;

  if aTarget.SetSize then
  begin
    Self.SizeWidth := aTarget.SizeWidth;
    Self.SizeHeight := aTarget.SizeHeight;
  end;
  if aTarget.AlwaysOnTop then
    SetWindowPos(fWndHnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE)
  else
    SetWindowPos(fWndHnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE);

  //if aPlaySound then
  //  MessageBeep(0);
end;

procedure TWindowData.Reload;
begin
  fTitle := '';
  fWindowClass := '';
  fExecutableName := '';
end;

procedure TWindowData.SetWindowPosition(aValue: TPoint);
begin
  SetWindowPos(fWndHnd, HWND_TOP, aValue.X, aValue.Y, 0, 0, SWP_NOSIZE or SWP_SHOWWINDOW);
end;

procedure TWindowData.SetWindowSize(aIndex, aValue: Integer);
begin
  if aIndex = 0 then
    SetWindowPos(fWndHnd, HWND_TOP, 0, 0, aValue, Self.SizeHeight, SWP_NOMOVE or SWP_SHOWWINDOW)
  else
    SetWindowPos(fWndHnd, HWND_TOP, 0, 0, Self.SizeWidth, aValue, SWP_NOMOVE or SWP_SHOWWINDOW);
end;

end.
