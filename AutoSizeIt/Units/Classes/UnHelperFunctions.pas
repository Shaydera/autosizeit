unit UnHelperFunctions;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Generics.Collections,
  UnWindowData, Vcl.Forms, shlobj, Xml.XMLIntf, Xml.XMLDoc, RTTI, UnTargetOptionData, System.Variants;

function EnumWindowsProc(aWnd: HWND; aWindows: TObjectList<TWindowData>): BOOL; stdcall;

function IsWindowHidden(aWindow: TWindowData): Boolean;

function FindWindowData(aWindowList: TObjectList<TWindowData>; aTitle, aClass: string): TWindowData; overload;
function FindWindowData(aWindowList: TObjectList<TWindowData>; aTarget: TTargetOptionData): TWindowData; overload;

procedure SetAlwaysOnTop(aHandle: HWND);
procedure RemoveAlwaysOnTop(aHandle: HWND);

function GetMyDocuments: string;
function LoadTargetSet(aFileName: string; aTargetSet: TObjectList<TTargetOptionData>): Boolean;
function SaveTargetSet(aFileName: string; aTargetSet: TObjectList<TTargetOptionData>): Boolean;


implementation

function IsWindowHidden(aWindow: TWindowData): Boolean;
const
  HidenClassNames: array[1..4] of string = ('Progman', 'CabinetWClass', 'CEF-OSC-WIDGET', 'Windows.UI.Core.CoreWindow');
var
  i: Integer;
begin
  Result := False;
  if (not IsWindowVisible(aWindow.WndHnd)) or (aWindow.Title = '') or (aWindow.WindowClass = '') then
  begin
    Exit(True);
  end;
  for i := 1 to Length(HidenClassNames) do
  begin
    if HidenClassNames[i] = aWindow.WindowClass then
      Exit(True);
  end;
end;


function EnumWindowsProc(aWnd: HWND; aWindows: TObjectList<TWindowData>): BOOL; stdcall;
var
  lWindow: TWindowData;
  lFound: Boolean;
  i: Integer;
begin
  Result := True; //Initially set return to true, otherwise iteration will stop.
  lWindow := TWindowData.Create(aWnd);
  try
    lFound := False;
    if IsWindowHidden(lWindow) or not Assigned(aWindows) then
    begin
      lWindow.Free;
      exit;
    end;

    for i := 0 to aWindows.Count-1 do
    begin
      if aWindows.Items[i].WndHnd = lWindow.WndHnd then
      begin
        lFound := True;
        break;
      end;
    end;
    if not lFound then
      aWindows.Add(lWindow);
  except
    on Ex : Exception do
    begin
      lWindow.Free;
      raise Ex;
    end;
  end;
end;


function FindWindowData(aWindowList: TObjectList<TWindowData>; aTitle, aClass: string): TWindowData;
var
  lWindowData: TWindowData;
begin
  Result := nil;
  for lWindowData in aWindowList do
  begin
    if (lWindowData.Title.Contains(aTitle)) and (lWindowData.WindowClass = aClass) then
    begin
      Result := lWindowData;
      break;
    end;
  end;
end;

function FindWindowData(aWindowList: TObjectList<TWindowData>; aTarget: TTargetOptionData): TWindowData;
var
  lWindowData: TWindowData;
begin
  Result := nil;
  for lWindowData in aWindowList do
  begin
    if aTarget.CompareMethod = TCompareMethod.Title then
    begin
      if lWindowData.Title.Contains(aTarget.Title) then
      begin
        Result := lWindowData;
        break;
      end;
    end
    else
    begin
      if lWindowData.WindowClass = aTarget.WindowClass then
      begin
        Result := lWindowData;
        break;
      end;
    end;
  end;
end;

procedure SetAlwaysOnTop(aHandle: HWND);
begin
  SetWindowPos(aHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE);
end;

procedure RemoveAlwaysOnTop(aHandle: HWND);
begin
  SetWindowPos(aHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE or SWP_NOMOVE);
end;

function GetMyDocuments: string;
var
  r: Boolean;
  lPath: array[0..255] of Char;
begin
  r := ShGetSpecialFolderPath(0, lPath, CSIDL_MYDOCUMENTS, False) ;
  if not r then
    raise Exception.Create('Could not find MyDocuments folder location.') ;
  Result := lPath;
end;

function LoadTargetSet(aFileName: string; aTargetSet: TObjectList<TTargetOptionData>): Boolean;
var
  lXmlDoc: IXMLDocument;
  lRoot,
  lNode,
  lTemp: IXMLNode;
  i,j: integer;
  lTargetData: TTargetOptionData;

  function TryGetAttribute(lAttributeName: string): OleVariant;
  begin
    try
      try
        Result := lTemp.Attributes[lAttributeName];
      finally
        if VarIsClear(Result) or VarIsEmpty(Result) or VarIsNull(Result) or (VarCompareValue(Result, Unassigned) = vrEqual) then
          Result := '';
      end;
    except
      Result := '';
    end;
  end;
begin
  if not (FileExists(aFileName)) or not Assigned(aTargetSet) then
    Exit(False);
  lXmlDoc := LoadXMLDocument(aFileName); //reference counted
  lRoot := lXmlDoc.DocumentElement;
  for i := 0 to lRoot.ChildNodes.Count - 1 do
  begin
    lNode := lRoot.ChildNodes[i];
    if lNode.LocalName = 'Targets' then
      for j := 0 to lNode.ChildNodes.Count - 1 do
      begin
        if not (lNode.ChildNodes[j].LocalName = 'TargetData') then
          Continue;
        lTemp := lNode.ChildNodes[j];
        lTargetData := TTargetOptionData.Create;
        lTargetData.Title := TryGetAttribute('Title');
        lTargetData.WindowClass := TryGetAttribute('WindowClass');
        lTargetData.ExecutableName := TryGetAttribute('ExecutableName');
        lTargetData.Action := TRttiEnumerationType.GetValue<TWindowAction>(TryGetAttribute('Action'));
        lTargetData.CompareMethod := TRttiEnumerationType.GetValue<TCompareMethod>(TryGetAttribute('CompareMethod'));
        lTargetData.SetSize := StrToBool(TryGetAttribute('SetSize'));
        lTargetData.SizeWidth := TryGetAttribute('SizeWidth');
        lTargetData.SizeHeight := TryGetAttribute('SizeHeight');
        lTemp := lTemp.ChildNodes['PositionOptions'];
        lTargetData.PositionOptions.ChangePosition := StrToBool(TryGetAttribute('ChangePosition'));
        lTargetData.PositionOptions.Center := StrToBool(TryGetAttribute('Center'));
        lTargetData.PositionOptions.PositionPt := TPoint.Create(TryGetAttribute('PosX'), TryGetAttribute('PosY'));
        aTargetSet.Add(lTargetData);
      end;
  end;
  lXmlDoc := nil; //early release reference counted IXMLDocument.
  Result := True;
end;

function SaveTargetSet(aFileName: string; aTargetSet: TObjectList<TTargetOptionData>): Boolean;
var
  lXmlDoc: IXMLDocument;
  lRootNode,
  lCurNode: IXMLNode;
  lTarget: TTargetOptionData;
begin
  try
    if aTargetSet.Count = 0 then
    begin
      if FileExists(aFileName) then
        DeleteFile(aFileName);
      Exit(True);
    end;

    lXmlDoc := NewXMLDocument();  //reference counted
    lXmlDoc.Encoding := 'utf-8';
    lXmlDoc.Options := [doNodeAutoIndent];

    lRootNode := lXmlDoc.AddChild('XML');
    lCurNode := lRootNode.AddChild('Targets');

    for lTarget in aTargetSet do
    begin
      lCurNode := lRootNode.ChildNodes['Targets'];
      lCurNode := lCurNode.AddChild('TargetData');
      lCurNode.Attributes['Title'] := lTarget.Title;
      lCurNode.Attributes['WindowClass'] := lTarget.WindowClass;
      lCurNode.Attributes['ExecutableName'] := lTarget.ExecutableName;
      lCurNode.Attributes['Action'] := TRttiEnumerationType.GetName(lTarget.Action);
      lCurNode.Attributes['CompareMethod'] := TRttiEnumerationType.GetName(lTarget.CompareMethod);
      lCurNode.Attributes['SetSize'] := BoolToStr(lTarget.SetSize, True);
      lCurNode.Attributes['SizeWidth'] := lTarget.SizeWidth;
      lCurNode.Attributes['SizeHeight'] := lTarget.SizeHeight;
      //TPositionData Class
      lCurNode := lCurNode.AddChild('PositionOptions');
      lCurNode.Attributes['ChangePosition'] := BoolToStr(lTarget.PositionOptions.ChangePosition, True);
      lCurNode.Attributes['Center'] := BoolToStr(lTarget.PositionOptions.Center, True);
      lCurNode.Attributes['PosX'] := lTarget.PositionOptions.PositionPt.X;
      lCurNode.Attributes['PosY'] := lTarget.PositionOptions.PositionPt.Y;
    end;

    if not DirectoryExists(ExtractFileDir(aFileName)) then
      CreateDir(ExtractFileDir(aFileName));

    lXmlDoc.SaveToFile(aFileName);
    lXmlDoc := nil; //early release reference counted IXMLDocument.
    Result := True;
  except
    on Ex: Exception do
    begin
      raise Ex;
    end;
  end;
end;

end.
