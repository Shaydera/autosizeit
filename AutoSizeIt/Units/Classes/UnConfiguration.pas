unit UnConfiguration;

interface

uses
  System.Types, System.Classes, System.SysUtils, System.Win.Registry, Winapi.Windows, Xml.XMLIntf, Xml.XMLDoc;

type
  TConfiguration = class(TObject)
  strict private
    fAutoStart,
    fWindowAlwaysTop,
    fPlaySoundOnAction: Boolean;
  public
    property AutoStart: Boolean read fAutoStart write fAutoStart;
    property WindowAlwaysTop: Boolean read fWindowAlwaysTop write fWindowAlwaysTop;
    property PlaySoundOnAction: Boolean read fPlaySoundOnAction write fPlaySoundOnAction;

    function Load(aFileName: string): Boolean;
    function Save(aFileName: string): Boolean;
    procedure SetAutoStart(aAppPath, aAppTitle: string; aRegister: Boolean);

    constructor Create(); reintroduce;
  end;

implementation

{ TConfiguration }

constructor TConfiguration.Create;
begin
  inherited;
  fAutoStart := True;
  fWindowAlwaysTop := True;
  fPlaySoundOnAction := False;
end;

function TConfiguration.Load(aFileName: string): Boolean;
var
  lXmlDoc: IXMLDocument;
  lRoot,
  lNode: IXMLNode;
  i: integer;
begin
  if not (FileExists(aFileName)) then
    exit(False);
  lXmlDoc := LoadXMLDocument(aFileName); //reference counted
  lRoot := lXmlDoc.DocumentElement;
  for i := 0 to lRoot.ChildNodes.Count - 1 do
  begin
    lNode := lRoot.ChildNodes[i];
    if lNode.LocalName = 'Options' then
    begin
      fAutoStart := lNode.Attributes['AutoStart'];
      fWindowAlwaysTop := lNode.Attributes['WindowAlwaysTop'];
      fPlaySoundOnAction := lNode.Attributes['PlaySoundOnAction'];
    end;
  end;
  lXmlDoc := nil; //early release reference counted IXMLDocument.
  Result := true;
end;

function TConfiguration.Save(aFileName: string): Boolean;
var
  lXmlDoc: IXMLDocument;
  lRootNode,
  lCurNode: IXMLNode;
begin
  try
    lXmlDoc := NewXMLDocument();  //reference counted
    lXmlDoc.Encoding := 'utf-8';
    lXmlDoc.Options := [doNodeAutoIndent];

    lRootNode := lXmlDoc.AddChild('XML');
    lCurNode := lRootNode.AddChild('Options');
    lCurNode.Attributes['AutoStart'] := fAutoStart;
    lCurNode.Attributes['WindowAlwaysTop'] := fWindowAlwaysTop;
    lCurNode.Attributes['PlaySoundOnAction'] := fPlaySoundOnAction;

    if not DirectoryExists(ExtractFileDir(aFileName)) then
      CreateDir(ExtractFileDir(aFileName));

    lXmlDoc.SaveToFile(aFileName);
    lXmlDoc := nil; //early release reference counted IXMLDocument.
    Result := True;
  except
    on Ex: Exception do
    begin
      raise Ex;
    end;
  end;
end;

procedure TConfiguration.SetAutoStart(aAppPath, aAppTitle: string; aRegister: Boolean);
const
  RegKey = '\Software\Microsoft\Windows\CurrentVersion\Run';
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    if Registry.OpenKey(RegKey, False) then
    begin
      if aRegister = False then
        Registry.DeleteValue(aAppTitle)
      else
        Registry.WriteString(aAppTitle, aAppPath);
    end
    else
      RaiseLastOSError;
  finally
    Registry.Free;
  end;
end;

end.
