unit UnTargetOptionData;

interface

uses
  System.SysUtils, Types;

type
  TWindowAction = (PositionResize, Maximize, Minimize);
  TCompareMethod = (Title, ClassName, Executable);
  TPositionMethod = (Point, Center, None);

  TPositionData = class
  strict private
    fPositionMethod: TPositionMethod;
    fPositionPt: TPoint;
    function GetChangePosition(): Boolean;
    function GetCenter(): Boolean;
    procedure SetChangePosition(aValue: Boolean);
    procedure SetCenter(aValue: Boolean);
  public
    property ChangePosition: Boolean read GetChangePosition write SetChangePosition;
    property Center: Boolean read GetCenter write SetCenter;
    property PositionPt: TPoint read fPositionPt write fPositionPt;
    constructor Create();
  end;

  TTargetOptionData = class
  strict private
    fTitle,
    fWindowClass,
    fExecutableName: string;
    fAction: TWindowAction;
    fCompareMethod: TCompareMethod;
    fSetSize: Boolean;
    fPositionResizeData: TPositionData;
    fSize: array[0..1] of Integer;
    fAlwaysTop: Boolean;
    function GetSize(aIndex: Integer): Integer;
    procedure ChangeSize(aIndex: Integer; aValue: Integer);
    function GetPositionResizeData(): TPositionData;
    procedure SetPositionResizeData(aValue: TPositionData);
    function GetActionAsString(): string;
    function GetMethodAsString(): string;
  public
    property Title: string read fTitle write fTitle;
    property WindowClass: string read fWindowClass write fWindowClass;
    property ExecutableName: string read fExecutableName write fExecutableName;
    property Action: TWindowAction read fAction write fAction;
    property CompareMethod: TCompareMethod read fCompareMethod write fCompareMethod;
    property SetSize: Boolean read fSetSize write fSetSize;
    property SizeWidth: Integer Index 0 read GetSize write ChangeSize;
    property SizeHeight: Integer index 1 read GetSize write ChangeSize;
    property PositionOptions: TPositionData read GetPositionResizeData write SetPositionResizeData;
    property ActionString: string read GetActionAsString;
    property MethodString: string read GetMethodAsString;
    property AlwaysOnTop: Boolean read fAlwaysTop write fAlwaysTop;
    constructor Create(); overload;
    constructor Create(aPositionOptions: TPositionData); overload;
    procedure Clear();
  end;


implementation

{ TPosResizeOptions }

constructor TPositionData.Create();
begin
  inherited;
  fPositionMethod := TPositionMethod.None;
  fPositionPt := TPoint.Create(0,0);
end;

{ TTargetOptionData }

function TTargetOptionData.GetSize(aIndex: Integer): Integer;
begin
  Result := fSize[aIndex];
end;

procedure TTargetOptionData.ChangeSize(aIndex: Integer; aValue: Integer);
begin
  fSize[aIndex] := aValue;
end;

constructor TTargetOptionData.Create;
begin
  inherited;
  Self.Clear();
end;

constructor TTargetOptionData.Create(aPositionOptions: TPositionData);
begin
  inherited Create;
  if Assigned(aPositionOptions) then
  begin
    if Assigned(fPositionResizeData) then
      fPositionResizeData.Free;
    fPositionResizeData := aPositionOptions
  end;
end;

procedure TTargetOptionData.Clear;
begin
  fTitle := '';
  fWindowClass := '';
  fExecutableName := '';
  fSize[0] := 0;
  fSize[1] := 0;
  fAction := TWindowAction.PositionResize;
  fCompareMethod := TCompareMethod.Title;
  fSetSize := False;
  fAlwaysTop := False;
  if Assigned(fPositionResizeData) then
    fPositionResizeData.Free;
  fPositionResizeData := TPositionData.Create;  
end;

function TTargetOptionData.GetActionAsString: string;
var
  lReturn: string;
begin
  lReturn := '';
  case fAction of
    TWindowAction.PositionResize: lReturn := 'Position / Resize';
    TWindowAction.Maximize: lReturn := 'Maximize';
    TWindowAction.Minimize: lReturn := 'Minimize';
  end;
  Result := lReturn;
end;

function TTargetOptionData.GetMethodAsString: string;
var
  lReturn: string;
begin
  lReturn := '';
  case fCompareMethod of
    TCompareMethod.Title: lReturn := 'Title';
    TCompareMethod.ClassName: lReturn := 'ClassName';
    TCompareMethod.Executable: lReturn := 'Executable';
  end;
  Result := lReturn;
end;

function TTargetOptionData.GetPositionResizeData: TPositionData;
begin
  if not Assigned(fPositionResizeData) then
    fPositionResizeData := TPositionData.Create();    
  Result := fPositionResizeData;
end;

procedure TTargetOptionData.SetPositionResizeData(aValue: TPositionData);
begin
  fPositionResizeData.Free;
  fPositionResizeData := aValue;  
end;

function TPositionData.GetCenter: Boolean;
begin
  Result := False;
  if fPositionMethod = TPositionMethod.Center then
    Result := True;  
end;

function TPositionData.GetChangePosition: Boolean;
begin
  Result := False;
  if fPositionMethod = TPositionMethod.Point then
    Result := True;
end;

procedure TPositionData.SetCenter(aValue: Boolean);
begin
  if aValue then
    fPositionMethod := TPositionMethod.Center
  else if not (fPositionMethod = TPositionMethod.Point) then
    fPositionMethod := TPositionMethod.None;
end;

procedure TPositionData.SetChangePosition(aValue: Boolean);
begin
  if aValue then
    fPositionMethod := TPositionMethod.Point    
  else if not (fPositionMethod = TPositionMethod.Center) then
    fPositionMethod := TPositionMethod.None;
end;

end.
