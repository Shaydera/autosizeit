unit UnWinApiExtensions;

interface

uses Winapi.Windows;

function GetModuleFileNameExW(
  hProcess: THandle;
  hModule: HINST;
  lpFilename: LPWSTR;
  nSize: Cardinal): Boolean; stdcall; external 'psapi.dll' name 'GetModuleFileNameExW';

implementation

end.
