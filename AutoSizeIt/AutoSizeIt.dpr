program AutoSizeIt;



uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  UnMain in 'Units\Forms\UnMain.pas' {FmMain},
  UnTargetOption in 'Units\Forms\UnTargetOption.pas' {FmTargetOption},
  UnConfiguration in 'Units\Classes\UnConfiguration.pas',
  UnTargetOptionData in 'Units\Classes\UnTargetOptionData.pas',
  UnWindowData in 'Units\Classes\UnWindowData.pas',
  UnOptionFrame in 'Units\Forms\UnOptionFrame.pas' {FmOptions},
  UnHelperFunctions in 'Units\Classes\UnHelperFunctions.pas',
  UnWinApiExtensions in 'Units\Classes\UnWinApiExtensions.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmMain, FmMain);
  TStyleManager.TrySetStyle('Windows10 SlateGray');
  Application.Run;
end.
